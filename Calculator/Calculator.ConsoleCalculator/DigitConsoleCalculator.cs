﻿using System;

namespace Calculator.ConsoleCalculator
{
    public class DigitConsoleCalculator
    {
        private readonly CalculatorLogic.Calculator _calculator;
        public string MemoryNumber { get; set; }

        public DigitConsoleCalculator(CalculatorLogic.Calculator calculator)
        {
            _calculator = calculator;
            MemoryNumber = "0";
        }
        public void ShowCalculator()
        {
            CalculatorManager.getInstance();
            Console.WriteLine(MemoryNumber);
            string operation = Console.ReadLine();
            if (operation.Equals("exit"))
            {
                Environment.Exit(0);
            }
            try
            {
                string number = Console.ReadLine();
                MemoryNumber = _calculator.Calculate(MemoryNumber, number, operation);
                Console.Clear();
            }
            catch (Exception exception)
            {
                Console.Clear();
                Console.WriteLine(exception.Message);
            }
            finally
            {
                ShowCalculator();
            }
        }
    }
}
