﻿using System;
using System.Collections.Generic;

namespace Calculator.CalculatorLogic.ReversePolishNotation
{
    public static class RpnHelper
    {
        public static double Calculate(string input)
        {
            var output = GetExpression(input);
            var result = Counting(output);
            return result;
        }

        private static string GetExpression(string input)
        {
            var output = string.Empty;
            var operStack = new Stack<char>();

            for (var i = 0; i < input.Length; i++)
            {
                if (IsDelimeter(input[i]))
                    continue;
                if (char.IsDigit(input[i]))
                {
                    while (!IsDelimeter(input[i]) && !IsOperator(input[i]))
                    {
                        output += input[i];
                        i++;

                        if (i == input.Length) break;
                    }

                    output += " ";
                    i--;
                }

                if (!IsOperator(input[i])) continue;
                switch (input[i])
                {
                    case '(':
                        operStack.Push(input[i]);
                        break;
                    case ')':
                        var s = operStack.Pop();

                        while (s != '(')
                        {
                            output += s.ToString() + ' ';
                            s = operStack.Pop();
                        }
                        break;
                    default:
                        if (operStack.Count > 0)
                            if (GetPriority(input[i]) <= GetPriority(operStack.Peek()))
                                output += operStack.Pop() + " ";

                        operStack.Push(char.Parse(input[i].ToString()));
                        break;
                }
            }

            while (operStack.Count > 0)
                output += operStack.Pop() + " ";

            return output;
        }

        private static double Counting(string input)
        {
            var temp = new Stack<double>();

            for (var i = 0; i < input.Length; i++)
                if (char.IsDigit(input[i]))
                {
                    var a = string.Empty;

                    while (!IsDelimeter(input[i]) && !IsOperator(input[i]))
                    {
                        a += input[i];
                        i++;
                        if (i == input.Length) break;
                    }
                    temp.Push(double.Parse(a));
                    i--;
                }
                else if (IsOperator(input[i]))
                {
                    var a = temp.Pop();
                    var b = temp.Pop();

                    double result;
                    switch (input[i])
                    {
                        case '+': result = b + a; break;
                        case '-': result = b - a; break;
                        case '*': result = b * a; break;
                        case '/': result = b / a; break;
                        default:
                            throw new InvalidOperationException();
                    }
                    temp.Push(result);
                }
            return temp.Peek();
        }

        private static bool IsDelimeter(char c)
        {
            return " =".IndexOf(c) != -1;
        }

        private static bool IsOperator(char с)
        {
            return "+-/*()".IndexOf(с) != -1;
        }

        private static byte GetPriority(char s)
        {
            switch (s)
            {
                case '(': return 0;
                case ')': return 1;
                case '+': return 2;
                case '-': return 3;
                case '*': return 4;
                case '/': return 4;
                default: return 5;
            }
        }
    }
}
