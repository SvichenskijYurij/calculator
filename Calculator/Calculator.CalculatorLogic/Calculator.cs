﻿namespace Calculator.CalculatorLogic
{
    public abstract class Calculator
    {
        public abstract string Calculate(string first, string second, string operation);
        public abstract string Calculate(string expression);
    }
}
