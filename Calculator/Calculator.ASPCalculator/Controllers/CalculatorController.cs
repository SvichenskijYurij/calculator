﻿using System;
using System.Web.Mvc;
using Calculator.ASPCalculator.Models;
using Calculator.CalculatorLogic;

namespace Calculator.ASPCalculator.Controllers
{
    public class CalculatorController : Controller
    {
        // GET: Calculator
        public ActionResult Index()
        {
            var model = new CalculatorModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(CalculatorModel model)
        {
            var calculator = CalculatorFactory.Create(CalculatorTypeEnum.Digit);
            string result;
            try
            {
                result = calculator.Calculate(model.Expression);
            }
            catch (Exception exception)
            {
                result = exception.Message;
            }
            model.Result = result;
            return View(model);
        }
    }
}