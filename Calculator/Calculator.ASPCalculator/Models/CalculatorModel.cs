﻿namespace Calculator.ASPCalculator.Models
{
    public class CalculatorModel
    {
        public string Expression { get; set; }
        public string Result { get; set; }

        public CalculatorModel()
        {
            Expression = string.Empty;
            Result = string.Empty;
        }
    }
}