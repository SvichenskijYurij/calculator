﻿namespace Calculator.WPFCalculator.View
{
    /// <summary>
    /// Логика взаимодействия для DisplayView.xaml
    /// </summary>
    public partial class DisplayView
    {
        public DisplayView()
        {
            InitializeComponent();
            DataContext = App.Manager.DisplayViewModel;
        }
    }
}
