﻿using System.Windows.Controls;

namespace Calculator.WPFCalculator.View
{
    /// <summary>
    /// Логика взаимодействия для ActionKeyboard.xaml
    /// </summary>
    public partial class ActionKeyboardView : UserControl
    {
        public ActionKeyboardView()
        {
            InitializeComponent();
            DataContext = App.Manager.ActionKeyboardViewModel;
        }
    }
}
