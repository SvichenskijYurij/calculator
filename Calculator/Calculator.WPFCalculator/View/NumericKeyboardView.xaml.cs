﻿using System.Windows.Controls;

namespace Calculator.WPFCalculator.View
{
    /// <summary>
    /// Логика взаимодействия для Keyboard.xaml
    /// </summary>
    public partial class NumericKeyboardView : UserControl
    {
        public NumericKeyboardView()
        {
            InitializeComponent();
            DataContext = App.Manager.NumericKeyboardViewModel;
        }
    }
}
