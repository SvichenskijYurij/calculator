﻿using System.Windows.Controls;

namespace Calculator.WPFCalculator.View
{
    /// <summary>
    /// Логика взаимодействия для DisplayKeyboardView.xaml
    /// </summary>
    public partial class DisplayKeyboardView : UserControl
    {
        public DisplayKeyboardView()
        {
            InitializeComponent();
            DataContext = App.Manager.DisplayKeyboardViewModel;
        }
    }
}
