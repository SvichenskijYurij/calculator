﻿namespace Calculator.WPFCalculator.Model
{
    public class DisplayModel : ModelBase
    {
        public DisplayModel()
        {
            _input = string.Empty;
            _memory = "0";
            _operation = string.Empty;
        }

        private string _input;

        public string Input
        {
            get { return _input; }
            set
            {
                _input = value;
                OnPropertyChanged();
            }
        }

        private string _memory;

        public string Memory
        {
            get { return _memory; }
            set
            {
                _memory = value;
                OnPropertyChanged();
            }
        }

        private string _operation;

        public string Operation
        {
            get { return _operation; }
            set
            {
                _operation = value;
                OnPropertyChanged();
            }
        }
    }
}
