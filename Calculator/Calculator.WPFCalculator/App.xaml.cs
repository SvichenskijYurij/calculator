﻿namespace Calculator.WPFCalculator
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App
    {
        public static ViewModelManager Manager => ViewModelManager.GetInstance();
    }
}
