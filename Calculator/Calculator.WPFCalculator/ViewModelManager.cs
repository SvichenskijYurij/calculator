﻿using Calculator.WPFCalculator.ViewModel;

namespace Calculator.WPFCalculator
{
    public class ViewModelManager
    {
        private ViewModelManager()
        {
            NumericKeyboardViewModel = new NumericKeyboardViewModel();
            ActionKeyboardViewModel = new ActionKeyboardViewModel();
            DisplayKeyboardViewModel = new DisplayKeyboardViewModel();
            DisplayViewModel = new DisplayViewModel();
        }

        private static ViewModelManager _instance;

        public NumericKeyboardViewModel NumericKeyboardViewModel { get; private set; }
        public ActionKeyboardViewModel ActionKeyboardViewModel { get; private set; }
        public DisplayKeyboardViewModel DisplayKeyboardViewModel { get; private set; }
        public DisplayViewModel DisplayViewModel { get; private set; }

        public static ViewModelManager GetInstance()
        {
            return _instance ?? (_instance = new ViewModelManager());
        }
    }
}
