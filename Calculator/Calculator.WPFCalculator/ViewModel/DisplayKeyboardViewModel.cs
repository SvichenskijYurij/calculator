﻿using Calculator.WPFCalculator.Command;
using System.Windows.Input;

namespace Calculator.WPFCalculator.ViewModel
{
    public class DisplayKeyboardViewModel
    {
        public DisplayKeyboardViewModel()
        {
            DisplayPressCommand = new RelayCommand(DisplayPress);
        }
        public ICommand DisplayPressCommand { get; set; }
        private void DisplayPress(object param)
        {
            if (param.ToString().Equals("Del"))
            {
                var input = App.Manager.DisplayViewModel.Model.Input;
                if (!string.IsNullOrEmpty(input))
                    App.Manager.DisplayViewModel.Model.Input = input.Remove(input.Length - 1, 1);
            }
            else
            {
                App.Manager.DisplayViewModel.Model.Input = string.Empty;
                App.Manager.DisplayViewModel.Model.Memory = "0";
                App.Manager.DisplayViewModel.Model.Operation = string.Empty;
            }
        }
    }
}
