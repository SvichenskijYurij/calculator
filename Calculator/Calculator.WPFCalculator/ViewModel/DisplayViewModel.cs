﻿using Calculator.WPFCalculator.Model;

namespace Calculator.WPFCalculator.ViewModel
{
    public class DisplayViewModel
    {
        public DisplayModel Model { get; set; }
        public DisplayViewModel()
        {
            Model = new DisplayModel();
        }
    }
}
