﻿using Calculator.WPFCalculator.Command;
using System.Windows.Input;

namespace Calculator.WPFCalculator.ViewModel
{
    public class NumericKeyboardViewModel
    {
        public NumericKeyboardViewModel()
        {
            NumberPressCommand = new RelayCommand(NumberPress);
        }

        public ICommand NumberPressCommand { get; set; }

        private void NumberPress(object param)
        {
            var length = App.Manager.DisplayViewModel.Model.Input.Length;
            if (length >= 10)
                return;
            App.Manager.DisplayViewModel.Model.Input += param.ToString();
        }
    }
}
