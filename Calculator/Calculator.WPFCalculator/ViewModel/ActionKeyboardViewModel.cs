﻿using Calculator.WPFCalculator.Command;
using System;
using System.Windows;
using System.Windows.Input;

namespace Calculator.WPFCalculator.ViewModel
{
    public class ActionKeyboardViewModel
    {
        private readonly CalculatorLogic.Calculator _calculator;
        public ActionKeyboardViewModel()
        {
            ActionPressCommand = new RelayCommand(ActionPress);
            _calculator = CalculatorLogic.CalculatorFactory.Create(CalculatorLogic.CalculatorTypeEnum.Digit);
        }
        public ICommand ActionPressCommand { get; set; }
        private void ActionPress(object param)
        {
            var action = param.ToString();
            if (action.Equals("="))
            {
                try
                {
                    var memory = App.Manager.DisplayViewModel.Model.Memory;
                    var input = App.Manager.DisplayViewModel.Model.Input;
                    var operation = App.Manager.DisplayViewModel.Model.Operation;
                    App.Manager.DisplayViewModel.Model.Memory = _calculator.Calculate(memory, input, operation);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                App.Manager.DisplayViewModel.Model.Operation = param.ToString();
        }
    }
}
