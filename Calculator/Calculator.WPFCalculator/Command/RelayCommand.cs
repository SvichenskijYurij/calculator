﻿using System;
using System.Windows.Input;

namespace Calculator.WPFCalculator.Command
{
    public class RelayCommand : ICommand
    {
        public RelayCommand(Action<object> execute)
        : this(execute, null) { }
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }
        public event EventHandler CanExecuteChanged;
        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke(parameter) ?? true;
        }
        public void Execute(object parameter)
        {
            _execute?.Invoke(parameter);
        }
        public void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
        private readonly Action<object> _execute;
        private readonly Predicate<object> _canExecute;
    }
}
